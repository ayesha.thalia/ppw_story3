from django.http import HttpResponse
from django.shortcuts import render, redirect

def index(request):
    response = {}
    return render(request,"index.html", response)

def quotes(request):
    response = {}
    return render(request, "quotes.html", response)

def moments(request):
    response = {}
    return render(request, "moments.html", response)